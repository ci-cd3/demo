package io.xyzshop.cloudnative.common;

import io.fabric8.kubernetes.api.model.ConfigMap;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.xyzshop.cloudnative.model.ClusterTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigMapUtils {
    private static Logger log = LoggerFactory.getLogger(ConfigMapUtils.class);

    /**
     * 从 configMap 中获取指定 Key 的配置数据
     *
     * @param clusterType
     * @param namespace
     * @param configName
     * @param key
     * @return
     */
    public static String getYamlFromConfigMap(ClusterTypeEnum clusterType, String namespace, String configName, String key) {
        try {
            KubernetesClient client = KubernetesClientUtils.getKubernetesClient(clusterType);
            ConfigMap configMap = client.configMaps().inNamespace(namespace).withName(configName).get();
            return configMap.getData().get(key);
        } catch (Exception e) {
            log.error(e.toString());
            return "";
        }
    }
}

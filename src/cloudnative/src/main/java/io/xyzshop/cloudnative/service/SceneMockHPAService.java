package io.xyzshop.cloudnative.service;

import com.google.common.base.Strings;
import io.fabric8.kubernetes.api.model.autoscaling.v2beta1.HorizontalPodAutoscaler;
import io.fabric8.kubernetes.api.model.autoscaling.v2beta1.HorizontalPodAutoscalerList;
import io.fabric8.kubernetes.api.model.batch.v1.Job;
import io.fabric8.kubernetes.api.model.batch.v1.JobList;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.dsl.MixedOperation;
import io.fabric8.kubernetes.client.dsl.Resource;
import io.fabric8.kubernetes.client.dsl.ScalableResource;
import io.xyzshop.cloudnative.common.CommonUtils;
import io.xyzshop.cloudnative.common.ConfigMapUtils;
import io.xyzshop.cloudnative.common.KubernetesClientUtils;
import io.xyzshop.cloudnative.model.ClusterTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;

@Service
public class SceneMockHPAService {
	private Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * 处理 pod hpa 模拟测试
	 *
	 * @param clusterType
	 * @param cpuTriggerType
	 * @param targetAverageValue
	 * @param minReplicas
	 * @param maxReplicas
	 * @return
	 */
	public boolean processMockPodsHPA(ClusterTypeEnum clusterType, int cpuTriggerType, float targetAverageValue, int minReplicas, int maxReplicas) {
		// 根据参数 创建 mall HPA 规则
		boolean success = createMallHPARule(clusterType, cpuTriggerType, targetAverageValue, minReplicas, maxReplicas);

		// 创建压测 k8s job 任务
		if (success) {
			success = createMallStressTestJob();
		}

		return success;
	}

	/**
	 * 根据参数 创建 mall HPA 规则
	 *
	 * @param clusterType
	 * @param cpuTriggerType
	 * @param targetAverageValue
	 * @param minReplicas
	 * @param maxReplicas
	 * @return
	 */
	private boolean createMallHPARule(ClusterTypeEnum clusterType, int cpuTriggerType, float targetAverageValue, int minReplicas, int maxReplicas) {
		try {
			// 获取 mallHPATemplate yaml
			String mallHPATemplate = ConfigMapUtils.getYamlFromConfigMap(clusterType, CommonUtils.NAMESPACE_XYZ, "mall-stress-test-config", "mallHPATemplate");

			// 根据 获取 mallHPATemplate yaml 创建 HPA 规则
			String metricName = calcHPAMetricName(cpuTriggerType);
			if (!Strings.isNullOrEmpty(metricName)) {
				// 计算 MALL HPA 规则名称、yaml 配置文件
				String mallHPAName = calcMallHPAName(cpuTriggerType);
				String mallHPAYaml = mallHPATemplate.replace("{mall-hpa-name}", mallHPAName)
						.replace("{minReplicas}", String.valueOf(minReplicas)).replace("{maxReplicas}", String.valueOf(maxReplicas))
						.replace("{metricName}", metricName).replace("{targetAverageValue}", String.valueOf(targetAverageValue));

				// 如果存在相同名称的 HPA 规则，则先删除此 HPA 规则
				KubernetesClient client = KubernetesClientUtils.getKubernetesClient(clusterType);
				MixedOperation<HorizontalPodAutoscaler, HorizontalPodAutoscalerList, Resource<HorizontalPodAutoscaler>> horizontalPodAutoscalers
						= client.autoscaling().v2beta1().horizontalPodAutoscalers();
				boolean deleted = deleteMallHPARule(horizontalPodAutoscalers, mallHPAName);
				if (deleted) {
					Thread.sleep(2000);
				}

				// 重新创建 mall HPA 规则
				HorizontalPodAutoscaler horizontalPodAutoscaler = horizontalPodAutoscalers.load(new ByteArrayInputStream(mallHPAYaml.getBytes())).get();
				horizontalPodAutoscalers.inNamespace(CommonUtils.NAMESPACE_XYZ).createOrReplace(horizontalPodAutoscaler);
			}
		} catch (Exception e) {
			log.error(e.toString());
			return false;
		}

		return true;
	}

	/**
	 * 根据 cpuTriggerType 参数，计算 CPU 指标触发类型
	 *
	 * @param cpuTriggerType
	 * @return
	 */
	private String calcHPAMetricName(int cpuTriggerType) {
		switch (cpuTriggerType) {
			case 0:
				return "k8s_pod_cpu_core_used";
			case 1:
				return "k8s_pod_rate_cpu_core_used_node";
			case 2:
				return "k8s_pod_rate_cpu_core_used_request";
			case 3:
				return "k8s_pod_rate_cpu_core_used_limit";
			default:
				return "";
		}
	}

	/**
	 * 计算 MallHPAName
	 *
	 * @param cpuTriggerType
	 * @return
	 */
	private String calcMallHPAName(int cpuTriggerType) {
		return "mall-hpa-trigger-" + cpuTriggerType;
	}

	/**
	 * 处理删除 mall HPA 规则
	 *
	 * @param horizontalPodAutoscalers
	 * @param mallHPAName
	 * @return
	 */
	private boolean deleteMallHPARule(MixedOperation<HorizontalPodAutoscaler, HorizontalPodAutoscalerList, Resource<HorizontalPodAutoscaler>> horizontalPodAutoscalers, String mallHPAName) {
		boolean deleted = false;

		HorizontalPodAutoscalerList hpaRuleList = horizontalPodAutoscalers.inNamespace(CommonUtils.NAMESPACE_XYZ).list();
		for (HorizontalPodAutoscaler hpaRule : hpaRuleList.getItems()) {
			if (hpaRule.getMetadata().getName().equals(mallHPAName)) {
				horizontalPodAutoscalers.inNamespace(CommonUtils.NAMESPACE_XYZ).withName(mallHPAName).delete();
				deleted = true;
			}
		}

		return deleted;
	}

	/**
	 * 创建 k8s job 压测任务
	 */
	private boolean createMallStressTestJob() {
		try {
			// 获取 mallHPATemplate yaml
			String mallStressTestJobYaml = ConfigMapUtils.getYamlFromConfigMap(ClusterTypeEnum.CLUSTER_AZ_GZ_4, CommonUtils.NAMESPACE_XYZ, "mall-stress-test-config", "mallStressTestYaml");

			// 若存在mallStressTest Job 工作负载，则先删除
			KubernetesClient client = KubernetesClientUtils.getKubernetesClient(ClusterTypeEnum.CLUSTER_AZ_GZ_4);
			MixedOperation<Job, JobList, ScalableResource<Job>> jobs = client.batch().v1().jobs();
			boolean deleted = deleteMallStressTestJob(jobs);
			if (deleted) {
				Thread.sleep(2000);
			}

			// 重新创建 mallStressTest Job 工作负载
			Job job = jobs.load(new ByteArrayInputStream(mallStressTestJobYaml.getBytes())).get();
			jobs.inNamespace(CommonUtils.NAMESPACE_XYZ).createOrReplace(job);
		} catch (Exception e) {
			log.error(e.toString());
			return false;
		}

		return true;
	}

	/**
	 * 删除 k8s job 压测任务
	 *
	 * @param jobs
	 * @return
	 */
	public boolean deleteMallStressTestJob(MixedOperation<Job, JobList, ScalableResource<Job>> jobs) {
		boolean deleted = false;

		JobList jobList = jobs.inNamespace(CommonUtils.NAMESPACE_XYZ).list();
		for (Job job : jobList.getItems()) {
			if (job.getMetadata().getName().equals("mall-stress-test")) {
				jobs.inNamespace(CommonUtils.NAMESPACE_XYZ).withName(job.getMetadata().getName()).delete();
				deleted = true;
			}
		}

		return deleted;
	}

	/**
	 * 处理删除 pod hpa 模拟测试配置
	 *
	 * @return
	 */
	public boolean processResetPodsHPA() {
		try {
			// 删除所有 HPA 规则
			for (int cpuTriggerType = 0; cpuTriggerType <= 3; cpuTriggerType++) {
				String mallHPAName = calcMallHPAName(cpuTriggerType);
				KubernetesClient client = KubernetesClientUtils.getKubernetesClient(ClusterTypeEnum.CLUSTER_AZ_GZ_4);
				MixedOperation<HorizontalPodAutoscaler, HorizontalPodAutoscalerList, Resource<HorizontalPodAutoscaler>> horizontalPodAutoscalers
						= client.autoscaling().v2beta1().horizontalPodAutoscalers();
				deleteMallHPARule(horizontalPodAutoscalers, mallHPAName);
			}

			// 删除所有 k8s job 压测任务
			KubernetesClient client = KubernetesClientUtils.getKubernetesClient(ClusterTypeEnum.CLUSTER_AZ_GZ_4);
			MixedOperation<Job, JobList, ScalableResource<Job>> jobs = client.batch().v1().jobs();
			deleteMallStressTestJob(jobs);

			// 重置 mall pod replicas 数量为 1
			client.apps().deployments().inNamespace(CommonUtils.calcNamespace(CommonUtils.DEPLOYMENT_MALL)).withName(CommonUtils.DEPLOYMENT_MALL).scale(1);
		} catch (Exception e) {
			log.error(e.toString());
			return false;
		}

		return true;
	}
}

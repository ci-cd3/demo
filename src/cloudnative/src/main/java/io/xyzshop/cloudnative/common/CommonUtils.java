package io.xyzshop.cloudnative.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

public class CommonUtils {
	private static Logger log = LoggerFactory.getLogger(CommonUtils.class);

	// K8S 常量
	public static final String NAMESPACE_XYZ = "demo-xyz";
	public static final String NAMESPACE_PDM = "demo-pdm";
	public static final String NAMESPACE_DB = "demo-db";

	public static final String POD_LABEL_SELECTOR_MALL = "app=mall";
	public static final String POD_LABEL_SELECTOR_ORDER = "app=order";
	public static final String POD_LABEL_SELECTOR_PASSPORT = "app=passport";
	public static final String POD_LABEL_SELECTOR_PRODUCT = "app=product";
	public static final String POD_LABEL_SELECTOR_REVIEW = "app=review";
	public static final String POD_LABEL_SELECTOR_SHOPCART = "app=shopcart";

	public static final String DEPLOYMENT_MALL = "mall";
	public static final String DEPLOYMENT_ORDER = "order";
	public static final String DEPLOYMENT_PASSPORT = "passport";
	public static final String DEPLOYMENT_PRODUCT_V1 = "product-v1";
	public static final String DEPLOYMENT_PRODUCT_V2 = "product-v2";
	public static final String DEPLOYMENT_REVIEW = "review";
	public static final String DEPLOYMENT_SHOPCART = "shopcart";

	// user session 名称
	public static final String USER_SESSION_NAME = "user_session";

	// APIVersion 版本号配置
	public static final String API_VERSION_V1 = "v1";
	public static final String API_VERSION_V2 = "v2";

	// 业务日志模板
	public static final String NO_DATA_TMPL = "%s, ";
	public static final String HAS_DATA_TMPL = "%s, [%s]";

	/**
	 * 根据 工作负载名称 计算命名空间名称
	 *
	 * @param workloadName
	 * @return
	 */
	public static String calcNamespace(String workloadName) {
		if (workloadName.contains("mysql") || workloadName.contains("redis")) {
			return NAMESPACE_DB;
		} else if (workloadName.contains("product") || workloadName.contains("review")) {
			return NAMESPACE_PDM;
		} else {
			return NAMESPACE_XYZ;
		}
	}

	/**
	 * 传递 headers 的 远程 GET 调用
	 *
	 * @param url          路径
	 * @param restTemplate 从controller 中传入的 restTemplate
	 * @param request      从controller 中传入的 request
	 * @return
	 */
	public static String restGet(String url, RestTemplate restTemplate, HttpServletRequest request) {
		HttpHeaders headers = traceHeaders(request);
		headers.add("consumer", "cloudnative");

		HttpEntity<Object> entity = new HttpEntity<>(headers);
		ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
		return result.getBody();
	}

	/**
	 * 完全传递上一个 request 的 headers
	 *
	 * @param request
	 * @return
	 */
	public static HttpHeaders traceHeaders(HttpServletRequest request) {
		return traceHeaders(request, null);
	}

	/**
	 * 组装 http request headers, 可以指定 headers 的 key，如果不指定，则完全传递前一个 request header。
	 *
	 * @param request 请求
	 * @param keys    指定keys
	 * @return headers
	 */
	public static HttpHeaders traceHeaders(HttpServletRequest request, String[] keys) {
		if (null == keys) {
			Enumeration<String> oriHeader = request.getHeaderNames();
			ArrayList<String> oriHeaderList = Collections.list(oriHeader);
			keys = oriHeaderList.toArray(new String[0]);
		}

		HttpHeaders headers = new HttpHeaders();
		for (String s : keys) {
			String v = request.getHeader(s);
			if (v != null && !v.isEmpty()) {
				headers.add(s, v);
			}
		}
		return headers;
	}
}

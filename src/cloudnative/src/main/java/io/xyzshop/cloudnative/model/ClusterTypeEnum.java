package io.xyzshop.cloudnative.model;

public enum ClusterTypeEnum {

    /**
     * 广州四区集群
     */
    CLUSTER_AZ_GZ_4,

    /**
     * 上海五区集群
     */
    CLUSTER_AZ_SH_5,
}

package io.xyzshop.cloudnative.model;

public class K8sPod {

    public K8sPod(int podNo, String podIp) {
        this.podNo = podNo;
        this.podIp = podIp;
    }

    /**
     * Pod 编号
     */
    private int podNo;

    /**
     * Pod Ip
     */
    private String podIp;

    public int getPodNo() {
        return podNo;
    }
    public void setPodNo(int podNo) {
        this.podNo = podNo;
    }

    public String getPodIp() {
        return podIp;
    }
    public void setPodIp(String podIp) {
        this.podIp = podIp;
    }
}

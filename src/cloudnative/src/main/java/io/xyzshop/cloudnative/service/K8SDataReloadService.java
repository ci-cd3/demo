package io.xyzshop.cloudnative.service;

import com.google.common.collect.Lists;
import io.fabric8.kubernetes.api.model.ContainerStatus;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodList;
import io.fabric8.kubernetes.api.model.Quantity;
import io.fabric8.kubernetes.api.model.metrics.v1beta1.ContainerMetrics;
import io.fabric8.kubernetes.api.model.metrics.v1beta1.PodMetrics;
import io.fabric8.kubernetes.api.model.metrics.v1beta1.PodMetricsList;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.xyzshop.cloudnative.common.CommonUtils;
import io.xyzshop.cloudnative.common.KubernetesClientUtils;
import io.xyzshop.cloudnative.model.ClusterTypeEnum;
import io.xyzshop.cloudnative.model.K8sPod;
import io.xyzshop.cloudnative.model.WorkloadMetric;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

@Service
public class K8SDataReloadService {
    private Logger log = LoggerFactory.getLogger(getClass());

    /**
     * 根据 namespace、labelSelector 获取 pod 列表
     *
     * @param clusterType
     * @param namespace
     * @param labelSelector
     * @return
     */
    public List<K8sPod> loadNamespacedPod(ClusterTypeEnum clusterType, String namespace, String labelSelector) {
        List<K8sPod> result = Lists.newArrayList();

        try {
            KubernetesClient client = KubernetesClientUtils.getKubernetesClient(clusterType);
            PodList list = client.pods().inNamespace(namespace).withLabel(labelSelector).list();

            int index = 0;
            for (int i = 0; i < list.getItems().size(); i++) {
                Pod pod = list.getItems().get(i);
                if (checkPodReady(pod)) {
                    index++;
                    result.add(new K8sPod(index, pod.getSpec().getNodeName()));
                }
            }
        } catch (Exception e) {
            log.error(e.toString());
        }

        return result;
    }

    /**
     * 检查一个 Pod 是否已经启动并准备好
     *
     * @param pod
     * @return
     */
    private boolean checkPodReady(Pod pod) {
        if (!pod.getStatus().getPhase().equals("Running")) {
            return false;
        }

        for (ContainerStatus containerStatus : pod.getStatus().getContainerStatuses()) {
            if (!containerStatus.getReady()) {
                return false;
            }
        }

        return true;
    }

    /**
     * 获取指定工作负载的监控数据（平均值）
     *
     * @param clusterType
     * @param workloadName
     * @return
     */
    public WorkloadMetric loadWorkloadMetric(ClusterTypeEnum clusterType, String workloadName) {
        WorkloadMetric workloadMetric = new WorkloadMetric();
        int totalCpu = 0;
        int totalMemory = 0;
        int podCount = 0;

        try {
            // 获取指定工作负载的监控数据
            KubernetesClient client = KubernetesClientUtils.getKubernetesClient(clusterType);
            PodMetricsList podMetricsList = client.top().pods().metrics(CommonUtils.calcNamespace(workloadName));

            for (PodMetrics podMetrics : podMetricsList.getItems()) {
                if (podMetrics.getMetadata().getName().startsWith(workloadName)) {
                    if (podMetrics.getContainers().size() > 0) {
                        podCount++;
                        for (ContainerMetrics containerMetrics : podMetrics.getContainers()) {
                            for (Map.Entry<String, Quantity> entry : containerMetrics.getUsage().entrySet()) {
                                if (entry.getKey().equals("cpu") && entry.getValue().getFormat().equals("m")) {
                                    totalCpu += Integer.valueOf(entry.getValue().getAmount());
                                } else if (entry.getKey().equals("memory") && entry.getValue().getFormat().equals("Ki")) {
                                    totalMemory += Integer.valueOf(entry.getValue().getAmount());
                                }
                            }
                        }
                    }
                }
            }

            // 计算指定工作负载的监控数据（平均值）
            if (podCount > 0) {
                workloadMetric.setAverageCpu(calcAverageCpu(totalCpu, podCount));
                workloadMetric.setAverageMemory(String.valueOf(totalMemory / podCount / 1000));
            }
        } catch (Exception e) {
            log.error(e.toString());
        }

        return workloadMetric;
    }

    /**
     * 计算平均 CPU 数据
     *
     * @param totalCpu
     * @param podCount
     * @return
     */
    private String calcAverageCpu(int totalCpu, int podCount) {
        DecimalFormat df = new DecimalFormat("0.000");
        return df.format(((double) totalCpu / 1000) / (double) podCount);
    }
}

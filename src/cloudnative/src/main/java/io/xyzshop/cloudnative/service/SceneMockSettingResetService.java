package io.xyzshop.cloudnative.service;

import io.xyzshop.cloudnative.model.ClusterTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SceneMockSettingResetService {
    private Logger log = LoggerFactory.getLogger(getClass());

    @Resource
    private SceneMockHPAService sceneMockHPAService;

    @Resource
    private SceneMockGrayscaleService sceneMockGrayscaleService;

    @Resource
    private SceneMockAZFailureService sceneMockAZFailureService;

    /**
     * 处理场景模拟系统配置重置
     *
     * @return
     */
    public boolean processSceneMockSettingReset() {
        // 恢复场景-1：突发流量 - Pod水平伸缩
        boolean resetHPAScene = sceneMockHPAService.processResetPodsHPA();

        // 恢复场景-2：服务治理 - 灰度发布
        boolean resetGrayscaleScene = sceneMockGrayscaleService.processMockProductGrayscale(100, 0);

        // 恢复场景-3：故障模拟 - 服务故障
        boolean resetAZFailureScene = sceneMockAZFailureService.processMockAZFailureOrRecovery(ClusterTypeEnum.CLUSTER_AZ_SH_5, false);

        return resetAZFailureScene && resetGrayscaleScene && resetHPAScene;
    }
}

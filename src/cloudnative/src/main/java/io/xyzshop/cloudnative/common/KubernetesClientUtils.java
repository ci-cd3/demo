package io.xyzshop.cloudnative.common;

import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.xyzshop.cloudnative.model.ClusterTypeEnum;
import me.snowdrop.istio.client.DefaultIstioClient;
import me.snowdrop.istio.client.IstioClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class KubernetesClientUtils {
    private static Logger log = LoggerFactory.getLogger(KubernetesClientUtils.class);
    private static KubernetesClient kubernetesclient_cluster1 = null;
    private static KubernetesClient kubernetesclient_cluster2 = null;
    private static IstioClient istioClient = null;
    private static String CLUSTER1_KUBE_CONFIG_FILE_PATH = "config/k8s-cluster1.config";
    private static String CLUSTER2_KUBE_CONFIG_FILE_PATH = "config/k8s-cluster2.config";

    /**
     * 根据集群类型获取 KubernetesClient 对象
     *
     * @param clusterType
     * @return
     */
    public static KubernetesClient getKubernetesClient(ClusterTypeEnum clusterType) {
        if (clusterType == ClusterTypeEnum.CLUSTER_AZ_GZ_4) {
            kubernetesclient_cluster1 = initKubernetesClient(kubernetesclient_cluster1, CLUSTER1_KUBE_CONFIG_FILE_PATH);
            return kubernetesclient_cluster1;
        } else {
            kubernetesclient_cluster2 = initKubernetesClient(kubernetesclient_cluster2, CLUSTER2_KUBE_CONFIG_FILE_PATH);
            return kubernetesclient_cluster2;
        }
    }

    /**
     * 初始化 k8s 集群客户端
     *
     * @param client
     * @param configFilePath
     * @return
     */
    private static KubernetesClient initKubernetesClient(KubernetesClient client, String configFilePath) {
        if (client == null) {
            try {
                Config config = getConfig(configFilePath);
                if (config != null) {
                    client = new DefaultKubernetesClient(config);
                }
            } catch (Exception e) {
                log.error(e.toString());
            }
        }

        return client;
    }

    /**
     * 获取 istio 网格客户端
     *
     * @return
     */
    public static IstioClient getIstioClient() {
        istioClient = initIstioClient(istioClient);
        return istioClient;
    }

    /**
     * 初始化 istio 网格客户端
     *
     * @param client
     * @return
     */
    private static IstioClient initIstioClient(IstioClient client) {
        if (client == null) {
            try {
                Config config = getConfig(CLUSTER1_KUBE_CONFIG_FILE_PATH);
                if (config != null) {
                    client = new DefaultIstioClient(config);
                }
            } catch (Exception e) {
                log.error(e.toString());
            }
        }

        return client;
    }

    /**
     * 获取 k8s 集群配置对象
     *
     * @param configFilePath
     * @return
     */
    private static Config getConfig(String configFilePath) {
        try {
            Resource resource = new ClassPathResource(configFilePath);
            String kubeConfig = new BufferedReader(new InputStreamReader(resource.getInputStream())).lines().collect(Collectors.joining(System.lineSeparator()));
            Config config = Config.fromKubeconfig(kubeConfig);
            config.setTrustCerts(true);
            return config;
        } catch (Exception e) {
            log.error(e.toString());
            return null;
        }
    }
}

package io.xyzshop.cloudnative.controller;

import com.google.common.collect.Lists;
import io.fabric8.kubernetes.api.model.ListOptions;
import io.xyzshop.cloudnative.common.CommonUtils;
import io.xyzshop.cloudnative.common.KubernetesClientUtils;
import io.xyzshop.cloudnative.model.ClusterTypeEnum;
import io.xyzshop.cloudnative.model.K8sPod;
import io.xyzshop.cloudnative.model.WorkloadMetric;
import io.xyzshop.cloudnative.service.K8SDataReloadService;
import io.xyzshop.cloudnative.service.SceneMockAZFailureService;
import me.snowdrop.istio.api.networking.v1alpha3.HTTPRouteDestination;
import me.snowdrop.istio.api.networking.v1alpha3.VirtualService;
import me.snowdrop.istio.api.networking.v1alpha3.VirtualServiceList;
import me.snowdrop.istio.client.IstioClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class K8SDataReloadController {

    @Resource
    private K8SDataReloadService k8SDataReloadService;

    @Resource
    private SceneMockAZFailureService sceneMockAZFailureService;

    @RequestMapping("/systemDataReload")
    public String systemDataReload(Model model) {
        WorkloadMetric workloadMetric = k8SDataReloadService.loadWorkloadMetric(ClusterTypeEnum.CLUSTER_AZ_GZ_4, "mall");
        model.addAttribute("averageCpu", workloadMetric.getAverageCpu());
        model.addAttribute("averageMemory", workloadMetric.getAverageMemory());

        return "template-main::system-fragment";
    }

    @RequestMapping("/archDataReload")
    public String archDataReload(Model model) {
        // 处理计算 mall pod 数量及前端显示样式
        mallPodsDataReload(model);

        // 处理计算 product 服务不同版本的灰度比例
        productGrayscaleProportionsDataReload(model);

        // 处理计算 可用区服务故障前端显示样式
        azFailureDataReload(model);

        return "template-main::arch-fragment";
    }

    /**
     * 处理计算 mall pod 数量及前端显示样式
     *
     * @param model
     */
    private void mallPodsDataReload(Model model) {
        List<K8sPod> pods_cluster_1 = k8SDataReloadService.loadNamespacedPod(ClusterTypeEnum.CLUSTER_AZ_GZ_4, CommonUtils.calcNamespace(CommonUtils.POD_LABEL_SELECTOR_MALL), CommonUtils.POD_LABEL_SELECTOR_MALL);
        List<K8sPod> pods_cluster_2 = Lists.newArrayList();
        pods_cluster_2.add(new K8sPod(1, ""));

        model.addAttribute("pods_cluster1", pods_cluster_1);
        model.addAttribute("podsStyle_cluster1", calcPodsStyle(pods_cluster_1.size()));

        model.addAttribute("pods_cluster2", pods_cluster_2);
        model.addAttribute("podsStyle_cluster2", calcPodsStyle(pods_cluster_2.size()));
    }

    /**
     * 根据 pod 数量计算 pods 前端页面样式
     *
     * @param count
     * @return
     */
    private String calcPodsStyle(int count) {
        switch (count) {
            case 1:
                return "padding-left: 150px";
            case 2:
                return "padding-left: 119px";
            case 3:
                return "padding-left: 82px";
            case 4:
                return "padding-left: 49px";
            case 5:
                return "padding-left: 8px";
            default:
                return "padding-left: 0";
        }
    }

    /**
     * 处理计算 product 服务不同版本的灰度比例
     *
     * @param model
     */
    private void productGrayscaleProportionsDataReload(Model model) {
        // 获取 istioClient
        IstioClient istioClient = KubernetesClientUtils.getIstioClient();

        // 获取 product 服务不同版本的灰度比例
        ListOptions listOptions = new ListOptions();
        listOptions.setLabelSelector("app=product");
        VirtualServiceList virtualServiceList = istioClient.v1alpha3VirtualService().list(listOptions);
        String v1GrayscaleProportionStr = "";
        String v2GrayscaleProportionStr = "";
        if (virtualServiceList.getItems().size() == 1) {
            VirtualService virtualService = virtualServiceList.getItems().get(0);
            if (virtualService.getSpec().getHttp().size() == 1) {
                List<HTTPRouteDestination> routes = virtualService.getSpec().getHttp().get(0).getRoute();
                if (routes.size() == 2) {
                    v1GrayscaleProportionStr = routes.get(0).getWeight() + "%";
                    v2GrayscaleProportionStr = routes.get(1).getWeight() + "%";
                }
            }
        }

        model.addAttribute("v1GrayscaleProportionStr", v1GrayscaleProportionStr);
        model.addAttribute("v2GrayscaleProportionStr", v2GrayscaleProportionStr);
    }

    /**
     * 处理计算 可用区服务故障 前端显示样式
     *
     * @param model
     */
    private void azFailureDataReload(Model model) {
        boolean azFailure = sceneMockAZFailureService.checkAZFailure(ClusterTypeEnum.CLUSTER_AZ_SH_5);
        model.addAttribute("azFailure", azFailure);
    }
}

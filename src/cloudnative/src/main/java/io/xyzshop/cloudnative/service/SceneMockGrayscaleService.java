package io.xyzshop.cloudnative.service;

import com.google.common.collect.Maps;
import io.xyzshop.cloudnative.common.KubernetesClientUtils;
import me.snowdrop.istio.api.networking.v1alpha3.VirtualService;
import me.snowdrop.istio.api.networking.v1alpha3.VirtualServiceBuilder;
import me.snowdrop.istio.client.IstioClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class SceneMockGrayscaleService {
    private Logger log = LoggerFactory.getLogger(getClass());

    /**
     * 处理 product 服务灰度发布
     *
     * @param v1GrayscaleProportion
     * @param v2GrayscaleProportion
     * @return
     */
    public boolean processMockProductGrayscale(int v1GrayscaleProportion, int v2GrayscaleProportion) {
        try {
            // 获取 istioClient
            IstioClient istioClient = KubernetesClientUtils.getIstioClient();

            // 根据输入的值创建 product-grayscale-vs 配置
            final String productHost = "product";
            Map<String, String> labelMap = Maps.newHashMap();
            labelMap.put("app", "product");
            final VirtualService virtualService = new VirtualServiceBuilder()
                    .withApiVersion("networking.istio.io/v1alpha3")
                    .withNewMetadata()
                    .withName("product-grayscale-vs")
                    .withNamespace("demo-xyz")
                    .withLabels(labelMap)
                    .endMetadata()
                    .withNewSpec()
                    .addToHosts(productHost)
                    .addNewHttp()
                    .addNewRoute()
                    .withNewDestination().withHost(productHost).withSubset("v1").endDestination()
                    .withWeight(v1GrayscaleProportion)
                    .endRoute()
                    .addNewRoute()
                    .withNewDestination().withHost(productHost).withSubset("v2").endDestination()
                    .withWeight(v2GrayscaleProportion)
                    .endRoute()
                    .endHttp()
                    .endSpec()
                    .build();

            // 更新 product-grayscale-vs 配置
            istioClient.v1alpha3VirtualService().delete(virtualService);
            Thread.sleep(1000);
            istioClient.v1alpha3VirtualService().createOrReplace(virtualService);
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }

        return true;
    }
}

package io.xyzshop.cloudnative.model;

public class WorkloadMetric {

    /**
     * 工作负载的平均 Cpu 数据（核）
     */
    private String averageCpu;

    /**
     * 工作负载的平均 Memory 数据（MB）
     */
    private String averageMemory;

    public String getAverageCpu() {
        return averageCpu;
    }

    public void setAverageCpu(String averageCpu) {
        this.averageCpu = averageCpu;
    }

    public String getAverageMemory() {
        return averageMemory;
    }

    public void setAverageMemory(String averageMemory) {
        this.averageMemory = averageMemory;
    }
}

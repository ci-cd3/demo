package io.xyzshop.cloudnative.controller;

import io.xyzshop.cloudnative.model.ClusterTypeEnum;
import io.xyzshop.cloudnative.service.SceneMockAZFailureService;
import io.xyzshop.cloudnative.service.SceneMockGrayscaleService;
import io.xyzshop.cloudnative.service.SceneMockHPAService;
import io.xyzshop.cloudnative.service.SceneMockSettingResetService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

@Controller
public class SceneMockController {

    @Resource
    private SceneMockHPAService sceneMockHPAService;

    @Resource
    private SceneMockGrayscaleService sceneMockGrayscaleService;

    @Resource
    private SceneMockAZFailureService sceneMockAZFailureService;

    @Resource
    private SceneMockSettingResetService sceneMockSettingResetService;

    @RequestMapping(value = "/mockPodsHPA/{cpuTriggerType}/{targetAverageValue}/{minReplicas}/{maxReplicas}", method = RequestMethod.GET)
    public String mockPodsHPA(@PathVariable("cpuTriggerType") Integer cpuTriggerType,
                              @PathVariable("targetAverageValue") Float targetAverageValue,
                              @PathVariable("minReplicas") Integer minReplicas,
                              @PathVariable("maxReplicas") Integer maxReplicas) {
        // 判断输入参数是否正确
        if (cpuTriggerType == null) {
            cpuTriggerType = 0;
        }
        if (targetAverageValue == null || targetAverageValue < 0) {
            targetAverageValue = 0F;
        }
        if (minReplicas == null || minReplicas <= 0) {
            minReplicas = 1;
        }
        if (maxReplicas == null || maxReplicas <= 0) {
            maxReplicas = 1;
        }

        // 调用服务方法，处理 HPA 动态伸缩
        sceneMockHPAService.processMockPodsHPA(ClusterTypeEnum.CLUSTER_AZ_GZ_4, cpuTriggerType, targetAverageValue, minReplicas, maxReplicas);
        return "template-main::arch-fragment";
    }

    @RequestMapping(value = "/mockProductGrayscale/{v1GrayscaleProportion}/{v2GrayscaleProportion}", method = RequestMethod.GET)
    public String mockProductGrayscale(@PathVariable("v1GrayscaleProportion") Integer v1GrayscaleProportion,
                                       @PathVariable("v2GrayscaleProportion") Integer v2GrayscaleProportion) {
        // 判断输入参数是否正确，正确则调用服务方法，处理 product 服务灰度发布
        if (v1GrayscaleProportion != null || v2GrayscaleProportion != null || (v1GrayscaleProportion + v2GrayscaleProportion == 100)) {
            sceneMockGrayscaleService.processMockProductGrayscale(v1GrayscaleProportion, v2GrayscaleProportion);
        }

        return "template-main::arch-fragment";
    }

    @RequestMapping(value = "/mockAZFailure", method = RequestMethod.GET)
    public String mockAZFailure() {
        sceneMockAZFailureService.processMockAZFailureOrRecovery(ClusterTypeEnum.CLUSTER_AZ_SH_5, true);
        return "template-main::arch-fragment";
    }

    @RequestMapping(value = "/resetSceneMockSetting", method = RequestMethod.GET)
    public String resetSceneMockSetting() {
        sceneMockSettingResetService.processSceneMockSettingReset();
        return "template-main::arch-fragment";
    }
}

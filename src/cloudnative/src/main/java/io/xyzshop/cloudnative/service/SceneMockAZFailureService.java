package io.xyzshop.cloudnative.service;

import com.google.common.collect.Lists;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.xyzshop.cloudnative.common.CommonUtils;
import io.xyzshop.cloudnative.common.KubernetesClientUtils;
import io.xyzshop.cloudnative.model.ClusterTypeEnum;
import io.xyzshop.cloudnative.model.K8sPod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SceneMockAZFailureService {
    private Logger log = LoggerFactory.getLogger(getClass());

    @Resource
    private K8SDataReloadService k8SDataReloadService;

    /**
     * 模拟 AZ 服务故障 或 服务故障
     *
     * @param failure
     * @return
     */
    public boolean processMockAZFailureOrRecovery(ClusterTypeEnum clusterType, boolean failure) {
        try {
            KubernetesClient client = KubernetesClientUtils.getKubernetesClient(clusterType);
            List<String> deploymentNames = calcDeploymentNames();
            for (String deploymentName : deploymentNames) {
                String namespace = CommonUtils.calcNamespace(deploymentName);
                client.apps().deployments().inNamespace(namespace).withName(deploymentName).scale(failure ? 0 : 1);
            }
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }

        return true;
    }

    /**
     * 检查某个集群是否处于可用区故障模拟状态
     *
     * @return
     */
    public boolean checkAZFailure(ClusterTypeEnum clusterType) {
        List<String> podLabelSelectors = calcPodLabelSelectors();
        for (String podLabelSelector : podLabelSelectors) {
            String namespace = CommonUtils.calcNamespace(podLabelSelector);
            List<K8sPod> k8sPods = k8SDataReloadService.loadNamespacedPod(clusterType, namespace, podLabelSelector);
            if (k8sPods.size() == 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * 计算所有系统 deployment names 数据
     *
     * @return
     */
    private List<String> calcDeploymentNames() {
        List<String> result = Lists.newArrayList();
        result.add(CommonUtils.DEPLOYMENT_MALL);
        result.add(CommonUtils.DEPLOYMENT_ORDER);
        result.add(CommonUtils.DEPLOYMENT_PASSPORT);
        result.add(CommonUtils.DEPLOYMENT_PRODUCT_V1);
        result.add(CommonUtils.DEPLOYMENT_PRODUCT_V2);
        result.add(CommonUtils.DEPLOYMENT_REVIEW);
        result.add(CommonUtils.DEPLOYMENT_SHOPCART);

        return result;
    }

    /**
     * 计算所有系统 pod labelSelectors 数据
     *
     * @return
     */
    private List<String> calcPodLabelSelectors() {
        List<String> result = Lists.newArrayList();
        result.add(CommonUtils.POD_LABEL_SELECTOR_MALL);
        result.add(CommonUtils.POD_LABEL_SELECTOR_ORDER);
        result.add(CommonUtils.POD_LABEL_SELECTOR_PASSPORT);
        result.add(CommonUtils.POD_LABEL_SELECTOR_PRODUCT);
        result.add(CommonUtils.POD_LABEL_SELECTOR_REVIEW);
        result.add(CommonUtils.POD_LABEL_SELECTOR_SHOPCART);

        return result;
    }
}

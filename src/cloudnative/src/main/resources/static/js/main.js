// 页面定时刷新获取最新数据
$(function () {
    reloadSystemData();
    reloadArchData();

    setInterval(function () {
        reloadSystemData();
    }, 2500);

    setInterval(function () {
        reloadArchData();
    }, 3500);
});

// reload data
function reloadSystemData() {
    $.ajax({
        url: '/systemDataReload',
        type: 'GET',
        success: function (data) {
            $("#system-fragment").html(data);
        }
    })
}

function reloadArchData() {
    $.ajax({
        url: '/archDataReload',
        type: 'GET',
        success: function (data) {
            $("#arch-fragment").html(data);
        }
    })
}

// scene mock trigger
$('#scene-mock-type').change(function () {
    var type = $(this).val();
    var sceneHpa = $("#scene-hpa");
    var sceneGrayscale = $("#scene-grayscale");
    var sceneAzFailure = $("#scene-az-failure");

    if (type == 0) {
        sceneHpa.show();
        sceneGrayscale.hide();
        sceneAzFailure.hide();
    } else if (type == 1) {
        sceneHpa.hide();
        sceneGrayscale.show();
        sceneAzFailure.hide();
    } else if (type == 2) {
        sceneHpa.hide();
        sceneGrayscale.hide();
        sceneAzFailure.show();
    }
})

// scene mock - 1: hpa
$('#cpu-trigger-type').change(function () {
    var data = $(this).val();
    $("#trigger-text").text((data == "0") ? "核" : "%")
})

$('#cpu-trigger-type').change(function () {
    var data = $(this).val();
    $("#trigger-text").text((data == "0") ? "核" : "%")
})

$('#btn-hpa').click(function () {
    var cpuTriggerType = $("#cpu-trigger-type").val();
    var targetAverageValue = $("#target-average-value").val();
    var minReplicas = $("#min-replicas").val();
    var maxReplicas = $("#max-replicas").val();
    var url = ['/mockPodsHPA', cpuTriggerType, targetAverageValue, minReplicas, maxReplicas].join('/');

    if (targetAverageValue == '') {
        alert('系统提示：请输入触发策略对应的触发值');
        return;
    }

    $.ajax({
        url: url,
        type: 'GET',
        success: function (data) {
            // alert(data);
        }
    })

    $("#btn-hpa").attr("disabled", "disabled").attr("class", "sumbit-btn-disabled");
    setTimeout(function () {
        $("#btn-hpa").removeAttr("disabled").attr("class", "sumbit-btn");
    }, 30000);
})

// scene mock - 2: grayscale
$('#btn-grayscale').click(function () {
    var v1GrayscaleProportion = $("#product-v1-grayscale-proportion").val();
    var v2GrayscaleProportion = $("#product-v2-grayscale-proportion").val();
    var url = ['/mockProductGrayscale', v1GrayscaleProportion, v2GrayscaleProportion].join('/');

    if (v1GrayscaleProportion == '' || v1GrayscaleProportion == ''
        || (parseInt(v1GrayscaleProportion) + parseInt(v2GrayscaleProportion) != 100)) {
        alert('系统提示：请输入正确的灰度发布设置（两个版本流量比例之和必须等于100%）');
        return;
    }

    $.ajax({
        url: url,
        type: 'GET',
        success: function (data) {
            // alert(data);
        }
    })

    $("#btn-grayscale").attr("disabled", "disabled").attr("class", "sumbit-btn-disabled");
    setTimeout(function () {
        $("#btn-grayscale").removeAttr("disabled").attr("class", "sumbit-btn");
    }, 2500);
})


// scene mock - 3: az-failure
$('#btn-az-failure').click(function () {
    var url = '/mockAZFailure';

    $.ajax({
        url: url,
        type: 'GET',
        success: function (data) {
            // alert(data);
        }
    })

    $("#btn-az-failure").attr("disabled", "disabled").attr("class", "sumbit-btn-disabled");
    setTimeout(function () {
        $("#btn-az-failure").removeAttr("disabled").attr("class", "sumbit-btn");
    }, 1000);
})


// scene mock reset
$('#btn-scene-mock-reset').click(function () {
    var url = '/resetSceneMockSetting';

    $.ajax({
        url: url,
        type: 'GET',
        success: function (data) {
            alert('系统配置重置成功！');
        }
    })

    $("#btn-scene-mock-reset").attr("disabled", "disabled").attr("class", "sumbit-btn-disabled");
    setTimeout(function () {
        $("#btn-scene-mock-reset").removeAttr("disabled").attr("class", "sumbit-btn");
    }, 2000);
})


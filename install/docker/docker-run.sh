ver=$1
cr="training.tencentcloudcr.com/xyzshop"


APP_NAME=xyzshop
DB_HOST='172.17.0.1'
DB_PASSWORD='P@ssword123'
DB_USERNAME='xyzshop_user'
DB_DATABASE='xyzshop'

REDIS_ADDRESS='172.17.0.1:6379'
REDIS_PASSWORD='red1sP@ss'
REDIS_DB=0

SERVICE_PASSPORT='http://172.17.0.1:5000'
SERVICE_PRODUCT='http://172.17.0.1:3000'
SERVICE_SHOPCART='http://172.17.0.1:6000'
SERVICE_ORDER='http://172.17.0.1:7000'
SERVICE_REVIEW='172.17.0.1:9000'

croot=$(pwd)


if [  -z $1 ]; then
  echo "命令行错误，请输入版本号。"
  echo "sh ./install/local/docker-run.sh 1.0"
  exit
fi

function label() {
  echo 
  echo 
  echo "--------------------------------------"
  echo ":: $1"
  echo "--------------------------------------"
  echo 
  echo 
}


function startMySQL(){
  if [ ! -z "$(docker ps -f name=xyz-mysql | grep xyz-mysql)" ]; then
    echo "MySQL 已经启动，跳过启动，如果要停止，运行"
    echo "docker stop xyz-mysql"
    return 1
  fi

  docker run --rm --name xyz-mysql --health-cmd='mysqladmin ping --silent' \
    -p 3306:3306 \
    -v "$(pwd)/install/db":/dumpdata \
    -e MYSQL_ROOT_PASSWORD=${DB_PASSWORD} -d \
    mysql:5.7 \
    --character-set-server=utf8mb4 \
    --collation-server=utf8mb4_unicode_ci \
    --skip-character-set-client-handshake \
    --default-authentication-plugin=mysql_native_password
  
  echo "MySQL 启动中，请稍等..."

  while status0=$(docker inspect --format "{{.State.Health.Status}}" xyz-mysql); [[ $status0 != "healthy" ]]; do 
    printf ">"
    sleep 1
    printf "\b=";
  done

  printf "\n\n启动完成，开始恢复数据\n"
  docker exec xyz-mysql sh -c "mysql -uroot -p${DB_PASSWORD} < /dumpdata/dump.sql"
  printf "\n\nMySQL 数据恢复完成\n"

}


###################################
label "启动 MySQL 引擎 (name=xyz-mysql)"
startMySQL


###################################
label "启动 redis 引擎 (name=xyz-redis)"
if [ ! -z "$(docker ps -f name=xyz-redis | grep xyz-redis)" ]; then
  echo "redis 已经启动，跳过启动，如果要停止，运行"
  echo "docker stop xyz-redis"
else
  docker run --rm --name=xyz-redis -d \
    -e REDIS_PASSWORD=${REDIS_PASSWORD} \
    -p 6379:6379 \
    redis:latest \
    sh -c 'exec redis-server --requirepass "$REDIS_PASSWORD"'

fi


###################################
label "启动 product"
if [ ! -z "$(docker ps -f name=product | grep product)" ]; then
  echo "product 已经启动，跳过启动，如果要停止，运行"
  echo "docker stop product"
else
docker run --rm --name=product -d \
  -p 3000:3000 \
  -e DB_HOST=${DB_HOST} \
  -e DB_PASSWORD=${DB_PASSWORD} \
  -e DB_USERNAME=${DB_USERNAME} \
  -e DB_DATABASE=${DB_DATABASE} \
  $cr/product:$ver \
  sh -c "java -jar product.jar"
fi




###################################
label "启动 review"

if [ ! -z "$(docker ps -f name=review | grep review)" ]; then
  echo "review 已经启动，跳过启动，如果要停止，运行"
  echo "docker stop review"
else

cat <<EOF > $(pwd)/.env
APP_NAME=${APP_NAME}
APP_KEY=base64:QHVH+p7eTaKkYNtJI5+0koXGH1FdXfFrLdj6N3KPdbM=
APP_DEBUG=true
APP_ENV=local
APP_URL=http://localhost:4000

LOG_CHANNEL=stack
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=${DB_HOST}
DB_PORT=3306
DB_DATABASE=${DB_DATABASE}
DB_USERNAME=${DB_USERNAME}
DB_PASSWORD=${DB_PASSWORD}
SERVICE_PASSPORT=${SERVICE_PASSPORT}

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DRIVER=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120
EOF

docker run --rm --name=review -d \
  -p 9000:9000 \
  -v "$(pwd)/.env":/app/.env \
  training.tencentcloudcr.com/xyzshop/review:$ver
fi


###################################
label "启动 mall"

if [ ! -z "$(docker ps -f name=mall | grep mall)" ]; then
  echo "mall 已经启动，跳过启动，如果要停止，运行"
  echo "docker stop mall"
else


cat <<EOF > $(pwd)/mall.conf
server { 
  listen       8000;
  index   index.html;
  root /app/public;

  location = /favicon.ico { access_log off; log_not_found off; }
  location = /robots.txt  { access_log off; log_not_found off; }

  location / {
    proxy_http_version 1.1;
    proxy_pass https://training-1251707795.cos.ap-shanghai.myqcloud.com;
    rewrite ^/(.*)$ /mall/index.html break;
  }

  location ^~/api/review/  {
    root /app/public;
    rewrite ^/api/review/(.*)$ /$1 break;
    try_files $uri $uri/ /index.php?$args;
  }

  location ~ \.php$ {
    set \$newurl \$request_uri;
    if (\$newurl ~ ^/api/review(.*)$) {
      set \$newurl \$1;
      root /app/public;
    }

    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_pass ${SERVICE_REVIEW};
    fastcgi_index index.php;
    include fastcgi_params;
    fastcgi_param REQUEST_URI \$newurl;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    fastcgi_intercept_errors off;
    fastcgi_buffer_size 16k;
    fastcgi_buffers 4 16k;
  }

  location ^~/api/passport/ {
    proxy_http_version 1.1;
    proxy_pass      ${SERVICE_PASSPORT}/;
  }
  location ^~/api/product/ {
    proxy_http_version 1.1;
    proxy_pass      ${SERVICE_PRODUCT}/;
  }
  
  location ^~/api/shopcart/ {
    proxy_http_version 1.1;
    proxy_pass      ${SERVICE_SHOPCART}/;
  }
  location ^~/api/order/ {
    proxy_http_version 1.1;
    proxy_pass      ${SERVICE_ORDER}/;
  }
}

EOF

docker run --rm --name=mall -d \
  -p 8000:8000 \
  -v "$(pwd)/mall.conf":/etc/nginx/conf.d/mall.conf \
  nginx:alpine 

fi


###################################
label "启动 passport"
if [ ! -z "$(docker ps -f name=passport | grep passport)" ]; then
  echo "passport 已经启动，跳过启动，如果要停止，运行"
  echo "docker stop passport"
else
docker run --rm --name=passport -d \
  -p 5000:5000 \
  -e EGG_SERVER_ENV=local \
  -e DB_HOST=${DB_HOST} \
  -e DB_PASSWORD=${DB_PASSWORD} \
  -e DB_USERNAME=${DB_USERNAME} \
  -e DB_DATABASE=${DB_DATABASE} \
  training.tencentcloudcr.com/xyzshop/passport:$ver  \
  sh -c "node index.js"
fi


###################################
label "启动 shopcart"
if [ ! -z "$(docker ps -f name=shopcart | grep shopcart)" ]; then
  echo "shopcart 已经启动，跳过启动，如果要停止，运行"
  echo "docker stop shopcart"
else
docker run --rm --name=shopcart -d \
  -p 6000:6000 \
  -e REDIS_ADDRESS=${REDIS_ADDRESS} \
  -e REDIS_PASSWORD=${REDIS_PASSWORD} \
  -e REDIS_DB=${REDIS_DB} \
  -e SERVICE_PRODUCT=${SERVICE_PRODUCT} \
  -e SERVICE_PASSPORT=${SERVICE_PASSPORT} \
  training.tencentcloudcr.com/xyzshop/shopcart:$ver  \
  sh -c "/app/main"
fi



###################################

label "启动 order"
if [ ! -z "$(docker ps -f name=order | grep order)" ]; then
  echo "order 已经启动，跳过启动，如果要停止，运行"
  echo "docker stop order"
else
docker run --rm --name=order -d \
  -p 7000:7000 \
  -e ConnectionStrings__OrderMySql="server=${DB_HOST};user=${DB_USERNAME};password=${DB_PASSWORD};database=${DB_DATABASE};old guids=true" \
  -e Providers__product=${SERVICE_PRODUCT} \
  -e Providers__passport=${SERVICE_PASSPORT} \
  training.tencentcloudcr.com/xyzshop/order:$ver  \
  ./order
fi

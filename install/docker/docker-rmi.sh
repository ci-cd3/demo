ver=$1
cr="training.tencentcloudcr.com/xyzshop"

if [  -z $1 ]; then
  echo "命令行错误，请输入版本号。"
  echo "sh ./install/local/docker-rmi.sh 1.0"
  exit
fi


function label() {
  echo 
  echo 
  echo "--------------------------------------"
  echo ":: 删除本地镜像 $1:$ver" 
  echo "--------------------------------------"
  echo 
  echo 
}


########################
label "mysql-xtrabackup"
docker rmi $cr/xtrabackup:$ver

########################
label "mysql-data"
docker rmi $cr/mysql-data:$ver

########################
label "product"
docker rmi $cr/product:$ver

########################
label "review"
docker rmi $cr/review:$ver

########################
label "php-runtime"
docker rmi $cr/php-runtime:$ver

########################
label "review-code"
docker rmi $cr/review-code:$ver

########################
label "passport"
docker rmi $cr/passport:$ver

########################
label "shopcart"
docker rmi $cr/shopcart:$ver

########################
label "order"
docker rmi $cr/order:$ver

########################
label "cloudnative"
docker rmi $cr/cloudnative:$ver
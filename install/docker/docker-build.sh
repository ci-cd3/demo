ver=$1
cr="yunxuetang.tencentcloudcr.com/yxt-demo"

if [ -z $1 ]; then
  echo "命令行错误，请输入版本号。"
  echo "sh ./install/local/docker-build.sh 1.0"
  exit
fi

croot=$(pwd)

function label() {
  echo 
  echo 
  echo "--------------------------------------"
  echo ":: 构建 $1"
  echo "--------------------------------------"
  echo 
  echo 
}

#####################
label "mysql-xtrabackup"
docker pull gcr.io/google-samples/xtrabackup:1.0
docker tag gcr.io/google-samples/xtrabackup:1.0 $cr/xtrabackup:$ver

########################
label "mysql-data"
cd $croot/install/db
docker build -t $cr/mysql-data:$ver .


#####################
label "product"
cd $croot/src/product
mvn clean package -DskipTests
docker build -t $cr/product:$ver .

########################
label "review"
cd $croot/src/review
docker build -t $cr/review:$ver .

########################
label "review php-runtime"
cd $croot/src/review
docker build -t $cr/php-runtime:$ver -f php-runtime.Dockerfile .

########################
label "review pure-code"
cd $croot/src/review
docker build -t $cr/review-code:$ver -f php-purecode.Dockerfile .


########################
label "passport"
cd $croot/src/passport
docker build -t $cr/passport:$ver .

########################
label "shopcart"
cd $croot/src/shopcart
docker build -t $cr/shopcart:$ver .

########################
label "order"
cd $croot/src/order/order
dotnet clean -c Release
dotnet publish -r linux-x64 -c Release -p:PublishSingleFile=true --self-contained false
docker build -t $cr/order:$ver .


#####################
label "cloudnative"
cd $croot/src/cloudnative
mvn clean package -DskipTests
docker build -t $cr/cloudnative:$ver .


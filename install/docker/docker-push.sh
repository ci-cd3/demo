ver=$1
cr="training.tencentcloudcr.com/xyzshop"
if [  -z $1 ]; then
  echo "命令行错误，请输入版本号。"
  echo "sh ./install/local/docker-push.sh 1.0"
  exit
fi

function push() {
  echo 
  echo 
  echo "-------------------------------------------------------------"
  echo ":: 推送  $cr/$1:$ver"
  echo "-------------------------------------------------------------"
  echo 
  echo 
  docker push $cr/$1:$ver
}

push xtrabackup

push mysql-data

push product

push review

push php-runtime

push review-code

push passport

push shopcart

push order

push cloudnative
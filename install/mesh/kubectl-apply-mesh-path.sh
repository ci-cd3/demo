# 在原来 kubernetes 部署的基础上进行 补丁 部署，使之成为一个 mesh 应用。
function label() {
  echo 
  echo 
  echo "--------------------------------------"
  echo ":: $1"
  echo "--------------------------------------"
  echo 
}


# Path 命名空间，加入自动注入
label "命名空间增加自动注入能力"
kubectl apply -f $(pwd)/install/mesh/ns.yaml

# Config，修改了 ng 的配置
label "修改配置"
kubectl apply -f $(pwd)/install/mesh/app-config.yaml

# 删除 pod 重启
label "重新部署 mall.xyz"
kubectl delete pod -l app=mall -n xyz
label "重新部署 shopcart.xyz"
kubectl delete pod -l app=shopcart -n xyz
label "重新部署 order.xyz"
kubectl delete pod -l app=order -n xyz
label "重新部署 passport.xyz"
kubectl delete pod -l app=passport -n xyz
label "重新部署 product.pdm"
kubectl delete pod -l app=product -n pdm

label "重新部署 review.pdm"
kubectl apply -f $(pwd)/install/mesh/review.yaml

label "增加 product-v2"
kubectl apply -f $(pwd)/install/mesh/product.yaml


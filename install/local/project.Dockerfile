FROM training.tencentcloudcr.com/xyzshop/training-os:2.0

RUN git clone -b main https://e.coding.net/joezou/cloudnative-training-camps/microservices-demo.git

WORKDIR /microservices-demo/src/review
RUN composer install

WORKDIR /microservices-demo/src/order/order
RUN dotnet restore

WORKDIR /microservices-demo/src/passport
RUN npm i

WORKDIR /microservices-demo/src/mall
RUN npm i

WORKDIR /microservices-demo/src/product
RUN mvn clean package -DskipTests

WORKDIR /microservices-demo/src/shopcart
RUN go get ./...

WORKDIR /microservices-demo
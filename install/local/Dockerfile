FROM docker:20

RUN apk update

RUN apk add bash git zsh wrk vim lsof \
  php7 php7-fpm php-cli php-phar php-xml php-mbstring php-json php-pdo php-pdo_mysql php-tokenizer php-dom php-fileinfo php-xmlwriter \
  openjdk8 \
  nodejs npm \
  go \
  icu-libs krb5-libs libgcc libintl libssl1.1 libstdc++ zlib


RUN mkdir /root/.kube

# [oh my zsh]
RUN sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
COPY ./zshrc /root/.zshrc

# [kubectl]
RUN wget -O /usr/local/bin/kubectl https://dl.k8s.io/release/v1.22.2/bin/linux/amd64/kubectl
RUN chmod 755 /usr/local/bin/kubectl

# [go]
# COPY --from=golang:alpine /usr/local/go/ /usr/local/go/
# ENV PATH="/usr/local/go/bin:${PATH}"


# [php composer]
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer


# [maven]
COPY ./apache-maven-3.8.2 /app/maven
ENV PATH="/app/maven/bin:${PATH}"


# [dotnet]
RUN apk add 
COPY ./dotnet /app/dotnet/
ENV PATH="/app/dotnet:${PATH}"
function label() {
  echo 
  echo 
  echo "--------------------------------------"
  echo ":: $1 启动中..."
  echo "--------------------------------------"
  echo 
  echo 
}
export APP_NAME=xyzshop
export DB_HOST='172.17.0.1'
export DB_PASSWORD='P@ssword123'
export DB_USERNAME='xyzshop_user'
export DB_DATABASE='xyzshop'

export REDIS_ADDRESS='172.17.0.1:6379'
export REDIS_PASSWORD='red1sP@ss'
export REDIS_DB=0

export SERVICE_PASSPORT='http://127.0.0.1:5000'
export SERVICE_PRODUCT='http://127.0.0.1:3000'
export SERVICE_SHOPCART='http://127.0.0.1:6000'
export SERVICE_ORDER='http://127.0.0.1:7000'
export SERVICE_REVIEW='http://127.0.0.1:4000'

# order(dotNet) setting
export ConnectionStrings__OrderMySql="server=${DB_HOST};user=${DB_USERNAME};password=${DB_PASSWORD};database=${DB_DATABASE};old guids=true"
export Providers__product=${SERVICE_PRODUCT}
export Providers__passport=${SERVICE_PASSPORT}


function startMySQL(){
  if [ ! -z "$(docker ps | grep xyz-mysql)" ]; then
    echo "MySQL 已经启动，跳过启动，如果要停止，运行"
    echo "docker stop xyz-mysql"
    return 1
  fi

  yes | cp -f $(pwd)/install/db/dump.sql /tmp/dump.sql

  docker run --rm --name xyz-mysql --health-cmd='mysqladmin ping --silent' \
    -p 3306:3306 \
    -v /tmp:/dumpdata \
    -e MYSQL_ROOT_PASSWORD=${DB_PASSWORD} -d \
    mysql:5.7 \
    --character-set-server=utf8mb4 \
    --collation-server=utf8mb4_unicode_ci \
    --skip-character-set-client-handshake \
    --default-authentication-plugin=mysql_native_password
  
  echo "MySQL 启动中，请稍等..."

  while STATUS=$(docker inspect --format "{{.State.Health.Status}}" xyz-mysql); [ $STATUS != "healthy" ]; do 
    printf ">"
    sleep 1
    printf "\b=";
  done

  printf "\n\n启动完成，开始恢复数据\n"
  docker exec xyz-mysql sh -c "mysql -uroot -p${DB_PASSWORD} < /dumpdata/dump.sql"
  printf "\n\nMySQL 数据恢复完成\n"

}


label "启动 MySQL 引擎 (name=xyz-mysql)"
startMySQL


label "启动 redis 引擎 (name=xyz-redis)"
if [ ! -z "$(docker ps | grep xyz-redis)" ]; then
  echo "redis 已经启动，跳过启动，如果要停止，运行"
  echo "docker stop xyz-redis"
else
docker run --rm --name=xyz-redis \
  -e REDIS_PASSWORD=${REDIS_PASSWORD} \
  -p 6379:6379 \
  -d redis \
  sh -c 'exec redis-server --requirepass "$REDIS_PASSWORD"'
fi


croot=$(pwd)

label "启动 review 服务"
cd $croot/src/review
cat <<EOF > ./.env
APP_NAME=${APP_NAME}
APP_KEY=base64:QHVH+p7eTaKkYNtJI5+0koXGH1FdXfFrLdj6N3KPdbM=
APP_DEBUG=true
APP_ENV=local
APP_URL=http://localhost:4000

LOG_CHANNEL=stack
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=${DB_HOST}
DB_PORT=3306
DB_DATABASE=${DB_DATABASE}
DB_USERNAME=${DB_USERNAME}
DB_PASSWORD=${DB_PASSWORD}
SERVICE_PASSPORT=${SERVICE_PASSPORT}

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DRIVER=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120
EOF

# composer install
php artisan serve --port=4000 &
sleep 3


label "启动 product 服务"
cd $croot/src/product
# mvn clean compile
# mvn exec:java &
# mvn clean package -DskipTests
java -jar target/product.jar &
sleep 10

label "启动 passport 服务"
cd $croot/src/passport
# npm install
node index.js &
sleep 4


label "启动 shopcart 服务"
cd $croot/src/shopcart
go run main.go &
sleep 4

label "启动 order 服务"
cd $croot/src/order/order
# dotnet restore
dotnet run &
sleep 4


label "启动 mall 服务"
cd $croot/src/mall
# npm install
npm start &
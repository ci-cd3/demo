#!/usr/bin/env bash

stopAppByPort() {
  printf "\n\n-------------------------------\n"
  printf ":: 停止端口 $1 服务"
  printf "\n-------------------------------\n"
  # lsof -i tcp:$1 | grep -v PID |  awk '{print $2}' | xargs ps -p | grep -v PID 
  # lsof -i tcp:$1 | grep -v PID |  awk '{print $2}' | xargs kill -9
  lsof -i:$1
  kill -9 $(lsof -t -i:$1 -sTCP:LISTEN)
}
stopDocker() {
  printf "\n\n-------------------------------\n"
  printf ":: 停止docker $1 "
  printf "\n-------------------------------\n"
  docker stop $1
}


# product
stopAppByPort 3000

# review
stopAppByPort 4000

# passport
stopAppByPort 5000

# shopcart
stopAppByPort 6000

# order
stopAppByPort 7000

# mall
stopAppByPort 8000


# stopDocker xyz-redis

# stopDocker xyz-mysql
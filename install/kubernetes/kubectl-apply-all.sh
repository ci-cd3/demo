#!/bin/bash
echo "Start to apply crd to xyz-shop ..."

function label() {
  echo 
  echo 
  echo "--------------------------------------"
  echo ":: $1"
  echo "--------------------------------------"
  echo 
}

#############
label "部署 Namespace"

if [[ "$1" == "mesh" ]]; then
  kubectl apply -f install/mesh/ns.yaml
else
  kubectl apply -f install/kubernetes/ns.yaml
fi


################
label "部署 ConfigMap 和 Secret"

if [[ "$1" == "mesh" ]]; then
  kubectl apply -f install/kubernetes/app-config.yaml
  kubectl apply -f install/mesh/app-config.yaml
else
  kubectl apply -f install/kubernetes/app-config.yaml
fi


label "部署 MySQL"
kubectl apply -f install/kubernetes/mysql.yaml
echo
echo "请等待 MySQL 启动完成"
while kubectl get pod mysql-2 -n db &> /dev/null; RESULT=$?; [ $RESULT -eq 1 ]; do
  printf ">"
  sleep 2
  printf "\b=";
done
while STATUS=$(kubectl get pod mysql-2 -n db -o=jsonpath="{.status.phase}"); [[ $STATUS != "Running" ]]; do 
  printf ">"
  sleep 2
  printf "\b=";
done


label "创建数据恢复 Job"
kubectl apply -f install/kubernetes/mysql-restore.yaml

echo
echo "请等待 MySQL 数据恢复"
while kubectl get job mysql-restore -n db &> /dev/null; RESULT=$?; [ $RESULT -eq 1 ]; do
  printf ">"
  sleep 1
  printf "\b=";
done
while STATUS=$(kubectl get job mysql-restore -n db -o=jsonpath="{.status.succeeded}"); [[ $STATUS != "1" ]]; do 
  printf ">"
  sleep 1
  printf "\b=";
done

####### 部署 mysql 完成
# exit 
#########
label "部署 redis"
kubectl apply -f install/kubernetes/redis.yaml

label "部署 product"
kubectl apply -f install/kubernetes/product.yaml
kubectl apply -f install/kubernetes/product-services.yaml

if [[ "$1" == "mesh" ]]; then
  kubectl apply -f install/mesh/product.yaml
fi

label "部署 review"
if [[ "$1" == "mesh" ]]; then
  kubectl apply -f install/mesh/review.yaml
else
  kubectl apply -f install/kubernetes/review.yaml
fi

label "部署 order"
kubectl apply -f install/kubernetes/order.yaml


label "部署 shopcart"
kubectl apply -f install/kubernetes/shopcart.yaml


label "部署 passport"
kubectl apply -f install/kubernetes/passport.yaml


label "部署 mall"
kubectl apply -f install/kubernetes/mall.yaml
kubectl apply -f install/kubernetes/mall-services.yaml
kubectl apply -f install/kubernetes/mall-ingress.yaml




echo
echo "部署完成"
echo
echo
echo "请等待分配入口 ip"
while ip=$(kubectl get ingress -n xyz -o=jsonpath="{.items[0].status.loadBalancer.ingress[0].ip}"); [[ -z $ip ]]; do
  printf ">"
  sleep 1
  printf "\b=";
done

echo
while STATUS=$(kubectl get pod -l=app=product -n pdm | grep Running); [[ -z $STATUS ]]; do 
  printf ">"
  sleep  1
  printf "\b=";
done
while STATUS=$(kubectl get pod -l=app=mall -n xyz | grep Running); [[ -z $STATUS ]]; do 
  printf ">"
  sleep  1
  printf "\b=";
done

echo "请打开 http://$ip"
# open http://$ip


